﻿namespace JamsNustache
{
    using System;
    using System.IO;
    using System.Net;

    using Nustache.Core;

    class Program
    {
        static void Main(string[] args)
        {
            var myObj = new
            {
                Member = new { firstName = "Tony" },
                ContactInfo = new { Phone = "888-360-SEAT (7328)", Email = "customerservice@flashseats.com" },
                TicketQuantity = "4 tickets",
                TicketsSold = "1 ticket sold.",
                Event = new { Name = "Game 2", Time = "Friday 10-02-15 at 7:00 pm CDT" },
                Venue = new { Name = "Quicken Loans Arena", City = "Cleveland", State = "OH" },
                OfferListing = new
                                   {
                                       ParkingIncluded = true,
                                       AskPrice = "$500.00",
                                       ExpiresDateTime = "Saturday 1-24-15 at 1:00 pm CST 2 days, 23 hours, and 59 minutes",
                                       ListingUrl = "http://cavs.qa3-fs.veritix.com/Default.aspx?ss=8&pid=24&ol=9000000000682186&fss=541830900"
                                   },
                SponsorMessageAvailable = true,
                SponsorMessage = "Are You a CavFanatic? Stand Up and Stand Out. Join the Cavaliers Social Network Today! www.cavfanatic.com",
                Copyright = "Copyright © 2006 - 2011 Flash Seats, LLC. Flash Seats and the Flash Seats logo are registered trademarks of Flash Seats, LLC. Use prohibited without written consent. All Rights Reserved.",
                AdUrl = "http://i.imgur.com/QcQ4Br7.jpg",
                AdUrl2 = "http://goo.gl/yEMV3U", //not sure what this is. haven't seen an example. this is a blank white image
                TransferAction = new { firstName = "Billy", lastName = "Blanks", toEmailAddress = "billy.blanks@veritix.com", initiateTransferDatetime = "Wednesday 11-26-14 at 10:17 am EST", TicketQuantity = "3 tickets", toContactPhoneNumber = "(330)-555-1234" }
            };

            const string OutputFile = "transferUnclaimedTransferor.html";

            var theTemplate = GetTemplateString("http://s3.amazonaws.com/veritix-dev3/email-notifications/flashseats/Transfer-Unclaimed-Transferor.nustache");
            var header = GetTemplateString("http://s3.amazonaws.com/veritix-dev3/email-notifications/flashseats/header.nustache");
            var footer = GetTemplateString("http://s3.amazonaws.com/veritix-dev3/email-notifications/flashseats/footer.nustache");

            var compiledTemplate = theTemplate.Replace("{{>header}}", header).Replace("{{>footer}}", footer);

            Render.StringToFile(compiledTemplate, myObj, OutputFile);

            Console.Out.WriteLine(Render.StringToString(compiledTemplate, myObj));

            Console.ReadLine();
        }

        private static string GetTemplateString(string url)
        {
            string result = string.Empty;

            try
            {
                var req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "Get";
                var resp = (HttpWebResponse)req.GetResponse();

                using (var stream = resp.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        result = sr.ReadToEnd();
                        sr.Close();
                    }
                }
            }
            catch (WebException wex)
            {
                result = wex.Message;
            }

            return result;
        }
    }
}
